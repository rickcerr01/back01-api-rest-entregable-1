package com.practitioner.entregable1;

import com.practitioner.entregable1.modelo.Producto;
import com.practitioner.entregable1.modelo.Usuario;
import com.practitioner.entregable1.servicio.impl.servicioProductoImpl;
import com.practitioner.entregable1.servicio.servicioProducto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Entregable1Application {

	public static void main(String[] args) {
		SpringApplication.run(Entregable1Application.class, args);
	}

	@Bean
	servicioProducto<Producto> servicioProducto() {
		return new servicioProductoImpl<Producto>();
	}

	@Bean
	servicioProducto<Usuario> servicioProveedor() {
		return new servicioProductoImpl<Usuario>();
	}

}
