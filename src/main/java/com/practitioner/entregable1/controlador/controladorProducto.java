package com.practitioner.entregable1.controlador;

import com.practitioner.entregable1.modelo.Producto;
import com.practitioner.entregable1.modelo.Usuario;
import com.practitioner.entregable1.servicio.servicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/almacen/v1/productos")
public class controladorProducto {
    @Autowired
    servicioProducto<Producto> servicioProducto;

    @Autowired
    EntityLinks entityLinks;

    @GetMapping("/productos/{idProducto}")
    public EntityModel<Producto> obtenerProductoPorId(@PathVariable(name = "idProducto") long id) {
        try {
            final Producto p = this.servicioProducto.obtenerPorId(id);
            return obtenerRespuestaProducto(p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/productos")
    public ResponseEntity<EntityModel<Producto>> crearProducto(@RequestBody Producto p) {

        final long id = this.servicioProducto.agregar(p);
        p.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceProducto(p).toUri())
                .body(obtenerRespuestaProducto(p))
                ;
    }

    @PutMapping("/productos/{idProducto}")
    public void reemplazarProductoPorId(@PathVariable(name = "idProducto") long id,
                                        @RequestBody Producto p) {
        try {
            p.setId(id);
            this.servicioProducto.reemplazarPorId(id, p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/productos/{idProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProductoPorId(@PathVariable(name = "idProducto") long id) {
        this.servicioProducto.borrarPorId(id);
    }

    @PatchMapping("/productos/{idProducto}")
    public void emparcharProductoPorId(@PathVariable(name = "idProducto") long id,
                                       @RequestBody ParcheProducto p) {
        try {

            final Producto o = this.servicioProducto.obtenerPorId(id);
            if(p.cantidad != null)
                o.setCantidad(p.cantidad);
            if(p.precio != null)
                o.setPrecio(p.precio);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int id){
        Producto pr = servicioProducto.obtenerPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.obtenerUsuarios()!=null)
            return ResponseEntity.ok(pr.obtenerUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/productos/{id}/user")
    public void agregarUsuarioProducto(@PathVariable long idProducto,
                                         @RequestBody UsuarioProducto prov) {
        try {
            this.servicioProducto.obtenerPorId(prov.idUsuario);
            this.servicioProducto.obtenerPorId(idProducto)
                    .obtenerIdsUsuarios().add(prov.idUsuario);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/productos/{id}/users/{indiceProveedorProducto}")
    public void borrarProveedorProducto(@PathVariable long idProducto,
                                        @PathVariable int indiceUsuarioProducto) {
        try {
            final List<Integer> listaIds = this.servicioProducto
                    .obtenerPorId(idProducto)
                    .obtenerIdsUsuarios();
            try {
                listaIds.remove(indiceUsuarioProducto);
            } catch(Exception x) {}
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    static class UsuarioProducto {
        public int idUsuario;
    }

    private Link crearEnlaceProducto(Producto p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este producto");
    }

    private List<Link> crearEnlacesAdicionalesProducto(Producto p) {
        return Arrays.asList(
                crearEnlaceProducto(p)
        );
    }

    private EntityModel<Producto> obtenerRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesAdicionalesProducto(p));
    }

    static class ParcheProducto {
        public Double precio;
        public Double cantidad;
    }
}
