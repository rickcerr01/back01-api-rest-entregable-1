package com.practitioner.entregable1.servicio.impl;

import com.practitioner.entregable1.servicio.servicioProducto;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class servicioProductoImpl<T> implements servicioProducto<T> {

    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final Map<Long, T> hashmap = new ConcurrentHashMap<>();

    @Override
    public long agregar(T p) {
        final long id = secuenciaIds.incrementAndGet();
        this.hashmap.put(id, p);
        return id;
    }

    @Override
    public T obtenerPorId(long id) {
        return this.hashmap.get(id);
    }

    @Override
    public void reemplazarPorId(long id, T p) {
        if(this.hashmap.replace(id, p) == null) {
            throw new RuntimeException("No existe el T con Id. ".concat(String.valueOf(id)));
        }
    }

    @Override
    public void borrarPorId(long id) {
        final T t = this.hashmap.get(id);
        this.hashmap.remove(id, t);
    }

    @Override
    public Map<Long, T> obtenerTodos() {
        return Map.copyOf(this.hashmap);
    }
}
