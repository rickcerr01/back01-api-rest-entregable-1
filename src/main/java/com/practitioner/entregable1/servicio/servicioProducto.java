package com.practitioner.entregable1.servicio;

import com.practitioner.entregable1.modelo.Usuario;

import java.util.List;
import java.util.Map;

public interface servicioProducto<T> {
    // CREATE
    public long agregar(T t);

    //READ
    public T obtenerPorId(long id);

    // READ []
    public Map<Long, T> obtenerTodos();

    // UPDATE
    public void reemplazarPorId(long id, T t);

    // DELETE
    public void borrarPorId(long id);
}
