package com.practitioner.entregable1.modelo;

import com.practitioner.entregable1.servicio.servicioProducto;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Producto {
    long id;
    String nombre;
    double precio;
    double cantidad;
    List<Usuario> idsUsuario;
    final List<Integer> idsUsuariosProducto = new ArrayList<>();

    public Producto(long id, String nombre, double precio, double cantidad) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public Producto(long id, String nombre, double precio, double cantidad, List<Usuario> usuarios) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.idsUsuario = usuarios;
    }

    @Autowired
    servicioProducto<Usuario> servicioUsuario;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public List<Usuario> obtenerUsuarios() {
        final Map<Long, Usuario> mp = this.servicioUsuario.obtenerTodos();
        final List<Usuario> pp = new ArrayList<>();
        for(Long id: mp.keySet()) {
            final Usuario p = mp.get(id);
            p.setId(id);
            pp.add(p);
        }
        return pp;
    }

    public List<Integer> obtenerIdsUsuarios() {
        return idsUsuariosProducto;
    }
}
